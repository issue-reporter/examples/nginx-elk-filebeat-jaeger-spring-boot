package com.gitlab.soshibby.issuereporter.demo.demoapp.controllers;

import com.gitlab.soshibby.issuereporter.demo.demoapp.configuration.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class DemoController {

    private static final Logger log = LoggerFactory.getLogger(DemoController.class);

    private RestTemplate restTemplate;
    private ServerConfig config;

    public DemoController(RestTemplate restTemplate, ServerConfig config) {
        this.restTemplate = restTemplate;
        this.config = config;
    }

    @RequestMapping("/ping")
    public String ping() {
        log.info("Ping");
        return "Pong";
    }

    @RequestMapping("/chaining")
    public String chaining(HttpServletRequest request) {
        log.info("Chaining Start.");
        ResponseEntity<String> response = restTemplate.getForEntity("http://" + config.getHost() + ":" + config.getPort() + "/another-chaining", String.class);
        log.info("Chaining End.");
        return "Chaining. " + response.getBody();
    }

    @RequestMapping("/another-chaining")
    public String anotherChaining(HttpServletRequest request) {
        log.info("Another chaining start.");
        ResponseEntity<String> response = restTemplate.getForEntity("http://" + config.getHost() + ":" + config.getPort() + "/leaf-chain", String.class);
        log.info("Another chaining end.");
        return "Another chaining. " + response.getBody();
    }

    @RequestMapping("/leaf-chain")
    public String leafChain(HttpServletRequest request) {
        log.info("Leaf chain start.");
        throw new RuntimeException("Throwing an example exception...");
    }

}
