load_module modules/ngx_http_opentracing_module.so;

events {}

http {
  opentracing on;

  opentracing_load_tracer /usr/local/lib/libjaegertracing_plugin.so /etc/jaeger-config.json;

  log_format  trace  '$remote_addr - $remote_user [$time_local] "$request" '
                               '$status $body_bytes_sent "$http_referer" '
                               '"$http_user_agent" "$upstream_http_trace_id" "$upstream_http_exception_span"';

  # Optionally, set additional tags.
  opentracing_tag http_user_agent $http_user_agent;

  upstream backend {
    server SERVER_IP:8090;
  }

  server {
    error_log /var/log/nginx/debug.log debug;
    listen 8080;
    server_name localhost;

    location ~ {
      # The operation name used for spans defaults to the name of the location
      # block, but you can use this directive to customize it.
      opentracing_operation_name $uri;

      # Propagate the active span context upstream, so that the trace can be
      # continued by the backend.
      # See http://opentracing.io/documentation/pages/api/cross-process-tracing.html
      opentracing_propagate_context;

      proxy_pass http://backend;
      access_log /var/log/nginx/access.log trace;
    }
  }
}
