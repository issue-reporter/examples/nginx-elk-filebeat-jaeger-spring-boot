## Installation
#### Install Ubuntu 
In this guide we are using a clean install of Ubuntu 18.10 ([download](http://releases.ubuntu.com/18.10/ubuntu-18.10-live-server-amd64.iso.torrent?_ga=2.256884046.479674560.1543682425-499739941.1539425174)) running inside VMWare.
But this should work with any new version of Ubuntu.

#### Install Java 8
```console
$ sudo apt-get install openjdk-8-jdk
```
#### Run spring-boot demo-app
```console
$ cd ~
$ git clone https://gitlab.com/issue-reporter/examples/nginx-elk-filebeat-jaeger-spring-boot.git
$ cd nginx-elk-filebeat-jaeger-spring-boot/demo-app
$ sudo vi src/main/resources/application.properties
```
Change the property jaeger.url to "http://localhost:14268".
```console
$ sudo chmod +x gradlew
$ sudo ./gradlew bootRun
```
Now open a browser and navigate to "http://UBUNTU_SERVER_IP:8090/ping" and you should see "Pong" in the browser.
Now leave this app running and start a new shell in your ubuntu server.

#### Install Docker
```console
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic test"
$ sudo apt-get update
$ sudo apt-get install docker-ce
$ sudo service docker start
```
Verify that Docker is installed correctly by running the hello-world image.
```console
$ sudo docker run hello-world
```
#### Install Docker Compose
```console
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```
Test the installation.
```console
$ docker-compose --version
```
You should get an output like this (note, version number might differ with yours):
```console
docker-compose version 1.23.1, build 1719ceb
```
#### Run nginx and Jaeger
```console
$ cd ~/nginx-elk-filebeat-jaeger-spring-boot/nginx
```
Change the nginx configuration so the proxy points to the demo-app service:
```console
$ sudo vi nginx.conf
```
On the line that says:
server SERVER_IP:8090;
Change SERVER_IP to the ip of your ubuntu server (i.e. the ip where the spring-boot app is hosted on).

Next we need to create a log file in /var/log, this log file will later be fetched and parsed by logstash and filebeat.
```console
$ sudo touch /var/log/docker-compose-nginx.log
$ sudo chmod 766 /var/log/docker-compose-nginx.log
```

Now start up Nginx and Jaeger by running:
```console
$ sudo docker-compose up > /var/log/docker-compose-nginx.log
```
If you navigate to the following address you should see the "Pong" in the browser.
http://UBUNTU_SERVER_IP:8080/ping
(replace UBUNTU_SERVER_IP with the ip to your ubuntu server)

If you now navigate to the following address in your browser you should see the Jaeger GUI.
http://UBUNTU_SERVER_IP:16686
(replace UBUNTU_SERVER_IP with the ip to your ubuntu server)

In the services drop-down list in the Jaeger GUI you should see the "nginx" service and "demo-app" showing up and if you click on "Find Traces" you should see the "ping" request showing up.

#### Install Elasticsearch
```console
$ sudo apt-get install apt-transport-https
$ echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
$ wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
$ sudo apt-get update && sudo apt-get install elasticsearch
$ sudo systemctl start elasticsearch
```
#### Install Kibana
```console
$ sudo apt-get update && sudo apt-get install kibana
```
To allow remote access to kibana do the following:
```console
$ sudo vi /etc/kibana/kibana.yml
```

Change "server.host" to "0.0.0.0".
```console
$ sudo systemctl start kibana
```

#### Install Logstash
```console
$ sudo apt-get update && sudo apt-get install logstash
```
Copy the configuration for parsing the nginx and demo-app logs. 
```console
$ sudo cp ~/nginx-elk-filebeat-jaeger-spring-boot/logstash/pipeline.conf /etc/logstash/conf.d/
```
Copy the patterns for parsing the nginx and demo-app logs.
```console
$ sudo cp -r ~/nginx-elk-filebeat-jaeger-spring-boot/logstash/patterns/ /etc/logstash/conf.d/
```
Start logstash:
```console
$ sudo service logstash start
```

#### Install Filebeat
```console
$ sudo apt-get update && sudo apt-get install filebeat
```
Make a backup of the default filebeat configuration.
```console
$ sudo cp /etc/filebeat/filebeat.yml /etc/filebeat/filebeat.yml.bak
```
Copy the configuration for reading Nginx and demo-app log files and shipping them to logstash.
```console
$ sudo cp ~/nginx-elk-filebeat-jaeger-spring-boot/filebeat/filebeat.yml /etc/filebeat/filebeat.yml
```
Start filebeat:
```console
$ sudo systemctl start filebeat
```

#### Install Issue Reporter & Plugins
```console
$ cd ~/nginx-elk-filebeat-jaeger-spring-boot
```
##### Install dependencies
Install log dependency:
```console
$ git clone https://gitlab.com/issue-reporter/issue-reporter-core/log.git
$ cd log
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
Install log-aggregator dependency:
```console
$ git clone https://gitlab.com/issue-reporter/issue-reporter-core/log-aggregator.git
$ cd log-aggregator
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
Install message-appender dependency:
```console
$ git clone https://gitlab.com/issue-reporter/issue-reporter-core/message-appender.git
$ cd message-appender
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
Install plugin-resolver dependency:
```console
$ git clone https://gitlab.com/issue-reporter/utils/plugin-resolver.git
$ cd plugin-resolver
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
##### Install plugins
Install elasticsearch-log-aggregator plugin:
```console
$ git clone https://gitlab.com/issue-reporter/log-aggregators/elasticsearch-log-aggregator.git
$ cd elasticsearch-log-aggregator
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
Install simple-string-appender plugin:
```console
$ git clone https://gitlab.com/issue-reporter/message-appenders/simple-string-appender.git
$ cd simple-string-appender
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
Install gitlab-issue-writer plugin:
```console
$ git clone https://gitlab.com/issue-reporter/issue-writers/gitlab-issue-writer.git
$ cd gitlab-issue-writer
$ sudo chmod +x gradlew
$ sudo ./gradlew install
$ cd ..
```
#### Install Issue reporter core application
```console
$ git clone https://gitlab.com/issue-reporter/issue-reporter-core/issue-reporter-core.git
$ cd issue-reporter-core
```
Copy configuration files:
```console
$ sudo mkdir build/libs/configs
$ sudo cp ../issue-reporter-configs/* build/libs/configs
```
Edit the configuration file for writing the issues to Gitlab:
```console
$ sudo vi build/libs/configs/issue-writer.yaml
```

Change "gitlabProjectId" and "accessToken" to your gitlab settings.

Next copy over the plugins:
```console
$ sudo mkdir plugins
$ sudo cp ../gitlab-issue-writer/build/libs/gitlab-issue-writer-1.0-SNAPSHOT.jar plugins/
$ sudo cp ../elasticsearch-log-aggregator/build/libs/elasticsearch-log-aggregator-1.0-SNAPSHOT.jar plugins/
$ sudo cp ../simple-string-appender/build/libs/simple-string-appender-1.0-SNAPSHOT.jar plugins/
```
Build issue-reporter-core application:
```console
$ sudo chmod +x gradlew
$ sudo ./gradlew build
$ cd build/libs
```
Start the application:
```console
$ sudo java -jar issue-reporter-core-all-1.0-SNAPSHOT.jar
```

Now if you go to your browser and enter "http://UBUNTU_SERVER_IP:8080/chaining" you should see an error like this:
```console
{"traceId":"d45ce4d272baa6fa","spanId":"8d07d1dea4ae7452","message":"Throwing an example exception..."}
```
and if you go to the project in Gitlab (the project id that you entered in the configuration above) you should see that a new issue has been created! You can change what information is added to the issue by editing the configuration files
```console
~/nginx-elk-filebeat-jaeger-spring-boot/issue-reporter-core/build/libs/configs/issue-build.yaml
```
```console
~/nginx-elk-filebeat-jaeger-spring-boot/issue-reporter-core/build/libs/configs/issue-note-build.yaml
```
